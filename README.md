# pdns-genrev

Tool to generate PTR records in reverse zones based off A/AAAA records in forward zones.

## Configuration

Requires the following environment variables to be set:

`GENREV_KEY` - the API key defined in the PowerDNS configuration

`GENREV_URL` - URL to the PowerDNS webserver, without a path (for example `http://[::1]:8081`)

`GENREV_ZONES` - comma separated list of forward zones to scan
